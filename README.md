# Vanity Accounts #

This program tries to find account numbers which match certain patterns.
It uses a randomly generated secret prefix as start point.

The following two modes are supported:

* looking for shortest numeric account number (default, always on)
* looking for accounts which start with words found in a provided dictionary file (option: "-d")


### Command line options ###

```
#!bash

java -jar Vanity.jar [ -d <dictionary file> <min_word_length> ]
```

### Dictionary ###

Dictionary format is one word per line. 
English and German dictionaries are available here https://hz-services.com/dictionaries/
Dictionary file can either be plain text or a zipped txt file. The character encoding of 
the text file must be UTF-8.

### Examples ###

```
#!bash
java -jar Vanity.jar
```
Without parameters vanity.jar is only looking for shortest numeric account number.


```
#!bash
java -jar Vanity.jar -d dictionary.txt.zip 8
```
Vanity.jar will search for shortest numeric account number and Reed-Solomon accounts beginning 
with words from the dictionary with a minimum length of 8 characters